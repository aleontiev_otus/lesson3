# natural_language_in_code - Natural language code analyzer

## Describe

The library allows you to analyze the source code and select from the definitions used words of the natural language.

## Install

- `git clone https://gitlab.com/aleontiev_otus/lesson3.git`
- `pip install -r requirements.txt`


## Quick start

 - Install requirements, see in file requirements.txt. 
 `pip install -r requirements.txt`
 - And download data for nltk `import nltk; nltk.download()`
 - Run example `python main.py -t function -ps verb -n 6 --repository github django/django`

## Functionality

### Configuration

To configure the module, you must call the object `Config(path, number, ast_type)`.


- **path**: The path or names of the repository to be analyzed
- **number**: Result row limit
- **ast_type**: Type of definitions to be analyzed `['function', 'name']`

#### Properties

- **config**: Get config as dict

#### Methods:

- **set_path(path)**: Update path for analysis

**Example:**
```
cfg = Config('.', 10, 'function')
print(cfg.config)

```

### Main

To use the basic functionality of the module, you must use the object
`Manager(cfg, part_speech, repository=False, output_format=False, output_path='.')`


- **cfg** (Required): Config object
- **part_speech** (Required): Part of speech for analysis `['verb', 'noun']`
- **repository**: Using the code repository for analysis `['github']`
- **output_format**: Formatting results `['csv', 'json', 'text']`
- **output_path**: The path to save the results

#### Functions

- get_repository(): Clone repositories
- get_result(): Get result data, list with tuple result `[(<word>, <count>), ... ]`.
- output_result(): Formatting and output the results

#### Example used

- **Local source code analysis**
```
    cfg = Config('.', 20, 'function')
    manager = Manager(cfg.config, 'verb')


    result = manager.get_result()
    manager.output_result(result)
```

- **Remote sourse code for analysis (github)**

```
    cfg = Config('django/django', 20, 'function')
    manager = Manager(cfg.config, 'verb', 'github')

    res = manager.get_repository()
    if res:
        cfg.set_path(res)
    else:
        sys.exit(2)

    result = manager.get_result()
    manager.output_result(result)
```

- **Local source code analysis, with output to result.txt use json format**

```
    cfg = Config('.', 20, 'function')
    manager = Manager(cfg.config, 'verb', output_format='json', output_path='result.txt')

    result = manager.get_result()
    manager.output_result(result)
```


## CLI mode

### Arguments

```
positional arguments:
  target                directory or repository name, to be analyze

optional arguments:
  -h, --help            show this help message and exit
  -n NUMBER, --number NUMBER
                        maximum number of rows to display the result
  -t {function,name}, --type {function,name}
                        the type of words to be analyzed (names of functions,
                        variables, etc)
  -ps {noun,verb}, --part-speech {noun,verb}
                        the part of speech that needs to be analyzed
  -o OUTPUT, --output OUTPUT
                        path for output result
  -f {csv,json,text}, --format {csv,json,text}
                        specify output format
  --repository {github}
                        type clone repository
  -v, --verbose         output extra information
  -d, --debug           turn on debug mode
  --version             show program's version number and exit
```

### Work with repositories for source code

The module supports code analysis in repositories by cloning the project to a temporary folder and then analyzing it.

**Supported repositories**: `github`.

**Example of the analysis of the repository of the Django project** -

`python main.py -t function -ps verb -n 6 --repository github django/django`


##  Structure

- formats : Formatters for data result
- repositories: Handlers for integration with repositories
- core: Main functions
- cli: Code for support cli mode
