import argparse
import logging
import sys

from natural_language_in_code import __version__, constants
from natural_language_in_code.core import Config, Manager

from natural_language_in_code.formats import formatters
from natural_language_in_code.repositories import repositories


LOG = logging.getLogger()


def _init_logger(debug=False):
    """Initialize the logger
    :param debug: debug mode
    """
    LOG.handlers = []
    log_level = logging.INFO
    if debug:
        log_level = logging.DEBUG

    logging.captureWarnings(True)
    LOG.setLevel(log_level)
    handler = logging.StreamHandler(sys.stderr)
    formatter = logging.Formatter(constants.log_format)
    handler.setFormatter(formatter)

    LOG.addHandler(handler)
    LOG.debug("logging initialized")


def __parse_args():
    """
    Add all cli arguments

    :return: None
    """

    parser = argparse.ArgumentParser(
        description='natural_language_in_code - the library allows you to analyze the source code' +
                    ' and select from the definitions used words of the natural language.',
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument(
        '-n', '--number', dest='number',
        action='store', default=10, type=int,
        help='maximum number of rows to display the result'
    )

    parser.add_argument(
        '-t', '--type', dest='type',
        action='store', default='function', type=str,
        choices=sorted(constants.ast_types.keys()),
        help='the type of words to be analyzed (names of functions, variables, etc)'
    )

    parser.add_argument(
        '-ps', '--part-speech', dest='part_speech',
        action='store', default='verb', type=str,
        choices=sorted(constants.nltk_tags.keys()),
        help='the part of speech that needs to be analyzed'
    )

    parser.add_argument(
        '-o', '--output', dest="output", type=str,
        help='path for output result'
    )

    output_format = 'text'
    formats = [f.alias for f in formatters]
    parser.add_argument(
        '-f', '--format', dest='format', action='store',
        default=output_format, help='specify output format',
        choices=sorted(formats)
    )

    repos = [f.alias for f in repositories]
    parser.add_argument(
        '--repository', action='store',
        help='type clone repository',
        choices=sorted(repos)
    )

    parser.add_argument(
        '-v', '--verbose', dest='verbose', action='store_true',
        help='output extra information'
    )
    parser.add_argument(
        '-d', '--debug', dest='debug', action='store_true',
        help='turn on debug mode'
    )

    parser.add_argument(
        'target', metavar='target', action='store',
        help='directory to be analyze'
    )

    python_ver = sys.version.replace('\n', '')
    parser.add_argument(
        '--version', action='version',
        version='%(prog)s {version}\n  python version = {python}'.format(
            version=__version__, python=python_ver)
    )

    parser.set_defaults(debug=False)
    parser.set_defaults(verbose=False)

    return parser.parse_args()


def main():
    debug = ('-d' in sys.argv or '--debug' in sys.argv)
    _init_logger(debug)
    args = __parse_args()

    if not args.target:
        LOG.error('No targets found in CLI or ini files, exiting.')
        sys.exit(2)

    cfg = Config(args.target, args.number, args.type)
    manager = Manager(cfg.config, args.part_speech, args.repository, args.format, args.output)

    if args.repository:
        res = manager.get_repository()
        if not res:
            LOG.error('Error clone repositories, check exist it for public users.')
            sys.exit(2)

        cfg.set_path(res)

    result = manager.get_result()
    manager.output_result(result)
