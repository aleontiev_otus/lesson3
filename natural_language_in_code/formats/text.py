from texttable import Texttable

from natural_language_in_code.formats.base import BaseFormatter


class TextFormatter(BaseFormatter):
    alias = 'text'

    def output(self, data):
        """
        Print data as pretty table

        :param list data: list with data
        :return: str
        """

        t = Texttable()
        t.add_row(['Word', 'Count'])

        for d in data:
            t.add_row(d)

        return t.draw()
