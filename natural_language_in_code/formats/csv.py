import csv
import io

from natural_language_in_code.formats.base import BaseFormatter


class CSVFormatter(BaseFormatter):
    alias = 'csv'

    def __write_data(self, fp, data, fieldnames):
        """
        Write data to csv file

        :param object fp: file handler
        :param list data: list with data
        :param list fieldnames: List with column names
        :return: None
        """

        writer = csv.DictWriter(fp, fieldnames)
        writer.writeheader()
        for d in data:
            writer.writerow({'word': d[0], 'count': d[1]})

    def output(self, data):
        """
        Convert data to csv and return

        :param list data: list with tuple
        :return: list
        """

        fieldnames = [
            'word',
            'count'
        ]

        fp = io.StringIO()
        self.__write_data(fp, data, fieldnames)

        return fp.getvalue()
