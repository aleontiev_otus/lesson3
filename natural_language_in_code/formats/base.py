import logging

LOG = logging.getLogger(__name__)


class BaseFormatter(object):
    alias = 'base'
    path = False
    log = LOG

    def set_output_path(self, path):
        self.path = path

    def output(self, data):
        LOG.info(self.alias)
