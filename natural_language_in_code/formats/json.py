import json

from natural_language_in_code.formats.base import BaseFormatter


class JSONFormatter(BaseFormatter):
    alias = 'json'

    def output(self, data):
        """
        Convert data to json and return as string

        :param list data: list with data
        :return: str
        """

        return json.dumps([{'word': d[0], 'count': d[1]} for d in data])
