from .base import BaseFormatter  # noqa:F401
from .csv import CSVFormatter  # noqa:F401
from .json import JSONFormatter  # noqa:F401
from .text import TextFormatter  # noqa:F401

__formatters = BaseFormatter.__subclasses__()
formatters = []
for forrmater in __formatters:
    formatters.append(forrmater())
