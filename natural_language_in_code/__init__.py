from natural_language_in_code.core import Config  # noqa:F401
from natural_language_in_code.core import Manager  # noqa:F401
import natural_language_in_code.core.constants as constants

__version__ = "0.0.2"
constants = constants
