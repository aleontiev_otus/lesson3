import logging
import ast
from nltk import pos_tag

LOG = logging.getLogger(__name__)


class Analyzer:

    def __init__(self, cfg, part):
        """

        :param dict cfg: configuration for package
        :param str part: part of speech that will be used in the analysis
        """

        self.tags = cfg['nltk_tags']
        self.path = cfg['path']
        self.find_part = part
        self.types = cfg['ast_types']

    def get_types(self):
        """
        Get supported ast_types

        :return: dict
        """

        return self.types

    def __is_part_of_speech(self, word, part):
        """
        Checking the word for identity to the part of speech

        :param str word: Word
        :param part: part of speech
        :return: bool
        """

        if not word or not self.tags[part]:
            return False

        pos_info = pos_tag([word])
        return pos_info[0][1] in self.tags[part]

    def get_defs(self, trees, type_node='name'):
        """

        :param trees:
        :param type_node:
        :return:
        """

        if type_node not in self.types:
            return []

        type_node = self.types[type_node]

        names = []
        for t in trees:
            if not t:
                continue

            for node in ast.walk(t):
                if not isinstance(node, type_node):
                    continue

                f = node.name.lower() if type_node != ast.Name else node.id
                if not f.startswith('__') and not f.endswith('__'):
                    names.append(f)

        return names

    def get_words(self, lists):
        """
        Separation names of the definitions to the words and filter by part of speech

        :param lists: list of definitions
        :return: list
        """
        words = []
        for name in lists:
            for _word in name.split('_'):
                if self.__is_part_of_speech(_word, self.find_part):
                    words.append(_word)

        return words
