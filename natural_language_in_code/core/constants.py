import ast

log_format = '%(asctime)-15s %(message)s'
nltk_tags = {
    'verb': ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ'],
    'noun': ['NN', 'NNS', 'NNP', 'NNPS']
}
filename_pattern = ['.py']
ast_types = {
    'name': ast.Name,
    'function': ast.FunctionDef
}
