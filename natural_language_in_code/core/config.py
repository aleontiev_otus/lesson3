from natural_language_in_code.core import constants


class Config(object):

    def __init__(self, path, number, ast_type):
        self._config = {}
        self._config['number'] = number if number else 10
        self._config['path'] = path if path else '.'
        self._config['type'] = ast_type if ast_type else 'function'
        self._config['ast_types'] = constants.ast_types
        self._config['nltk_tags'] = constants.nltk_tags
        self._config['filename_pattern'] = constants.filename_pattern
        self._config['log_format_string'] = constants.log_format

    def set_path(self, path):
        self._config['path'] = path

    @property
    def config(self):
        """
        Get config as dict
        :return: dict
        """
        return self._config
