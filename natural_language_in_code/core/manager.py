import logging
import os
import collections


from natural_language_in_code.core import Analyzer, Parser
import natural_language_in_code.formats as OutputFormatters
import natural_language_in_code.repositories as Repositories

LOG = logging.getLogger(__name__)


class Manager:
    def __init__(self, cfg, part_speech, repository=False,
                 output_format=False, output_path=False):
        self.cfg = cfg
        self.part_speech = part_speech
        self.repository = repository
        self.output_path = output_path
        self.output_format = output_format

        LOG.debug('Init manager')
        LOG.debug('Config:')
        LOG.debug(self.cfg)
        LOG.debug('Other parameters:')
        LOG.debug({k: self.__dict__[k] for k in self.__dict__ if k != 'cfg'})

    def get_result(self):
        """
        To run code analysis based on the project settings. The result is a list of words and their frequency.

        :return: collection
        """

        parser = Parser(self.cfg['path'], self.cfg['filename_pattern'])
        analyzer = Analyzer(self.cfg, self.part_speech)

        trees = parser.get_trees()
        names = analyzer.get_defs(trees, self.cfg['type'])

        if not names:
            return []

        names = analyzer.get_words(names)

        words = []
        for f in names:
            words += f.split('_')

        return collections.Counter(words).most_common(self.cfg['number'])

    def get_repository(self):
        """
        Clone repository for temporary dir, and return path.

        :return: str
        """

        handler = False
        for r in Repositories.repositories:
            if r.alias == self.repository:
                handler = r

        if not handler:
            LOG.error(f'Not found handler for repository {self.repository}')
            return

        return handler.get_code(self.cfg['path'])

    def output_result(self, data):
        """
        The generation and output/save of data

        :param collections data: collection for words and their frequency
        :return: None
        """

        if not self.output_format:
            LOG.debug('Not set output format')
            return

        formatter = False
        for _forrmater in OutputFormatters.formatters:
            if _forrmater.alias == self.output_format:
                formatter = _forrmater

        if not formatter:
            LOG.debug('Not found formatter for result')
            return

        out = formatter.output(data)

        if self.output_path:
            self.__save_result(out)
            return

        print(out)

    def __save_result(self, data):
        """
        Save formatted data to file

        :param str|bytes data: Data to save
        :return: None
        """

        try:
            with open(self.output_path, 'w') as fp:
                fp.write(data)
                LOG.info('Result data write to file {p}'.format(
                    p=os.path.abspath(self.output_path)
                ))
        except:  # noqa:E722
            LOG.error('Error save result data to output file. ' +
                      f'Please check path - {self.output_path}')
