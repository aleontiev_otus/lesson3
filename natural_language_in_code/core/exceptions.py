class NotCreateTempDir(BaseException):
    """ Exception for situations where the temporary folder could not be created """

    __traceback__ = False

    def __init__(self, msg=None):
        if msg is None:
            msg = "Could not create temporary folder for source code"
        super(NotCreateTempDir, self).__init__(msg)
