import os
import logging
import ast

LOG = logging.getLogger(__name__)


class Parser:
    def __init__(self, path, patterns):
        """

        :param path: path for source codes
        :param patterns: patterns for filename, which will analize
        """

        self.path = path
        self.patterns = patterns

    def get_trees(self, with_filename=False, with_file_content=False):
        """
        Get ast tree for analysis

        :param bool with_filename: Add the name of the file
        :param with_file_content: Add the content of the file
        :return: list
        """

        trees = []
        filenames = self.get_all_files()
        LOG.info('total found %s files' % len(filenames))

        for filename in filenames:
            with open(filename, 'r', encoding='utf-8') as attempt_handler:
                main_file_content = attempt_handler.read()

            try:
                tree = ast.parse(main_file_content)
            except SyntaxError as e:
                LOG.info(e)
                continue

            row = []

            if with_filename:
                row.append(filename)

            if with_file_content:
                row.append(main_file_content)

            row.append(tree)
            trees.append(tuple(row) if len(row) > 1 else row[0])

        LOG.debug('trees generated')
        return trees

    def __check_file_pattern(self, filename):
        """
        Check file name for pattern matching

        :param str filename: file name
        :return: bool
        """

        for pattern in self.patterns:
            if filename.endswith(pattern):
                return True
        return False

    def get_all_files(self):
        """
        Get all files for analysis, filtered by extension
        :return: list
        """

        filenames = []

        for dirname, dirs, files in os.walk(self.path, topdown=True):
            for file in files:
                if not self.__check_file_pattern(file):
                    continue

                filenames.append(os.path.join(dirname, file))
                if len(filenames) == 100:
                    break

        return filenames
