from tempfile import mkdtemp

from natural_language_in_code.core.exceptions import NotCreateTempDir


class BaseRepository(object):

    base_link = ''
    alias = ''

    def __init__(self):
        try:
            self.path = mkdtemp()
        except:  # noqa:E722
            raise NotCreateTempDir()

    def get_code(self, path_repos):
        raise NotImplemented
