from natural_language_in_code.repositories.base import BaseRepository
from git import Repo, GitCommandError


class Github(BaseRepository):

    def __init__(self):
        self.base_link = 'https://github.com'
        self.alias = 'github'
        super(Github, self).__init__()

    def get_code(self, path_repos):
        """
        Clone repositories in temporary path and return path for sources

        :param path_repos: name repositories
        :return: str
        """

        if not path_repos or not self.path:
            return False

        if not path_repos.endswith('.git'):
            path_repos = path_repos + '.git'

        try:
            res = Repo.clone_from(
                f'{self.base_link}/{path_repos}',
                to_path=self.path)
        except GitCommandError:
            return False

        if not res:
            return False

        return self.path
