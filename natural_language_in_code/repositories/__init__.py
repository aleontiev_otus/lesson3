from .base import BaseRepository
from .github import Github  # noqa:F401

repositories = []
for cls in BaseRepository.__subclasses__():
    repositories.append(cls())
